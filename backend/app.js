import { usersData } from './mockData.js';

import pkg from 'express';

const app = pkg();
const port = 4200;

app.get('/', (req, res) => {
	res.send('¡Hola desde el servidor Express!');
});

app.get('/user', (req, res) => {
	const userData = usersData[0];
	res.json(userData);
});

app.get('/users', (req, res) => {
	res.json(usersData);
});

app.get('/status', (req, res) => {
	const userData = {
		status: 'On',
	};
	res.json(userData);
});

app.listen(port, () => {
	console.log(`Servidor Express escuchando en el port ${port}`);
});
