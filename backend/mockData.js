export const usersData = [
	{
		nombre: 'María García',
		email: 'maria@example.com',
		edad: 28,
		ocupacion: 'Ingeniera de Software',
	},
	{
		nombre: 'Juan Rodríguez',
		email: 'juan@example.com',
		edad: 34,
		ocupacion: 'Diseñador Gráfico',
	},
	{
		nombre: 'Laura Pérez',
		email: 'laura@example.com',
		edad: 25,
		ocupacion: 'Analista de Datos',
	},
	{
		nombre: 'Carlos López',
		email: 'carlos@example.com',
		edad: 30,
		ocupacion: 'Médico',
	},
	{
		nombre: 'Ana Martínez',
		email: 'ana@example.com',
		edad: 27,
		ocupacion: 'Profesora',
	},
	{
		nombre: 'Pedro González',
		email: 'pedro@example.com',
		edad: 32,
		ocupacion: 'Abogado',
	},
	{
		nombre: 'Sofía Fernández',
		email: 'sofia@example.com',
		edad: 29,
		ocupacion: 'Chef',
	},
	{
		nombre: 'Diego Torres',
		email: 'diego@example.com',
		edad: 35,
		ocupacion: 'Arquitecto',
	},
	{
		nombre: 'Valentina Ramírez',
		email: 'valentina@example.com',
		edad: 26,
		ocupacion: 'Ingeniera Civil',
	},
	{
		nombre: 'Luisa Silva',
		email: 'luisa@example.com',
		edad: 31,
		ocupacion: 'Periodista',
	},
];
